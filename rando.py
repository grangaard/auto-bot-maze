import argparse
from random import randint
from runner import Runner

# note:  don't call it 'random.py', bad things will happen with imports

parser = argparse.ArgumentParser(description='generate a random maze')
parser.add_argument('--seed', metavar='s', dest= 'seed', type=int,
                    help='integer seed for maze generation')
parser.add_argument('--width', metavar='w', dest='width', type=int,
                    default=35, help='maze width')
parser.add_argument('--height', metavar='y', dest='height', type=int,
                    default=21, help='maze height')
parser.add_argument('--complexity', metavar='c', dest='complexity', type=float,
                    default=0.75, help='maze complexity')
parser.add_argument('--density', metavar='d', dest='density', type=float,
                    default=0.75, help='density for the maze')
args = parser.parse_args()
args_dict = vars(args)

def callback(position):
    move = randint(0,3)
    if move == 0:
        return 'N'
    elif move == 1:
        return 'S'
    elif move == 2:
        return 'E'
    elif move == 3:
        return 'W'

app = Runner(callback, **args_dict)
app.run()
