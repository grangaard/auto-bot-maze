import argparse
from pygame.locals import *
from runner import Runner

parser = argparse.ArgumentParser(description='generate a random maze')
parser.add_argument('--seed', metavar='s', dest= 'seed', type=int,
                    help='integer seed for maze generation')
parser.add_argument('--width', metavar='w', dest='width', type=int,
                    default=35, help='maze width')
parser.add_argument('--height', metavar='y', dest='height', type=int,
                    default=21, help='maze height')
parser.add_argument('--complexity', metavar='c', dest='complexity', type=float,
                    default=0.75, help='maze complexity')
parser.add_argument('--density', metavar='d', dest='density', type=float,
                    default=0.75, help='density for the maze')
args = parser.parse_args()
args_dict = vars(args)

def callback(position):
    keys = position.pygame.key.get_pressed()
    if (keys[K_RIGHT]):
        return 'E'
    elif (keys[K_LEFT]):
        return 'W'
    elif (keys[K_UP]):
        return 'N'
    elif (keys[K_DOWN]):
        return 'S'
    else:
        return ''

app = Runner(callback, **args_dict)
app.run()
