from maze import Maze
from numpy.random import randint
from pygame.locals import *
import pygame

class Position:
    x = None
    y = None

    _maze = None
    pygame = None
    
    def __init__(self, maze, x, y):
        self.x = x
        self.y = y
        self._maze = maze
        
    def north(self):
        if self._maze.maze[self.y, self.x - 1] == 1:
            return False
        return True

    def south(self):
        if self._maze.maze[self.y, self.x + 1] == 1:
            return False
        return True
    
    def east(self):
        if self._maze.maze[self.y + 1, self.x] == 1:
            return False
        return True

    def west(self):
        if self._maze.maze[self.y - 1, self.x] == 1:
            return False
        return True
    
class Runner:
    pygame = None
    _maze = None
    position = None
    
    callback = None

    _scale = 32
    
    windowWidth = None
    windowHeight = None
    
    def __init__(self, callback, width, height, complexity, density, seed=None):
        self._running = True
        self._display_surf = None
        self._image_surf = None
        self._block_surf = None
        self._door_sulf = None
        self.callback = callback
        self._maze = Maze(width, height, complexity, density, seed)
        self.windowWidth = self._maze.width * self._scale
        self.windowHeight = self._maze.height * self._scale
        # pick a starting point
        x = randint(1, width - 1)
        y = randint(1, height - 1)
        # while we're stuck in a wall...
        while self._maze.maze[y,x]:
            # shift randomly left or right (or maybe stay put)
            x = x + randint(-1, 1)
            # make sure we didn't run off the edge
            if x < 1:
                x = 1
            if x >= width:
                x = width - 1
            # shift up or down (or maybe stay put)
            y = y + randint(-1, 1)
            # make sure we didn't run off the edge
            if y < 1:
                y = 1
            if y >= height:
                y = height - 1
        self.position = Position(self._maze, x, y)
        print('random seed =', self._maze.seed)
        print('exit is at', self._maze.exit_x, ',', self._maze.exit_y)
        print('bot is at', x, ',', y)
        
    def init(self):
        pygame.init()
        self.pygame = pygame
        self.position.pygame = pygame
        self._display_surf = pygame.display.set_mode((self.windowWidth,self.windowHeight), pygame.HWSURFACE)
        pygame.display.set_caption('Auto Robot Maze')
        self._running = True
        self._image_surf = pygame.image.load("robot.png").convert()
        self._block_surf = pygame.image.load("block.png").convert()
        self._door_surf = pygame.image.load("door.png").convert()

    def render(self):
        self._display_surf.fill((0,0,0))
        self._display_surf.blit(self._image_surf,
                                (self.position.x * self._scale,
                                 self.position.y * self._scale))
        for x in range(0, self._maze.width):
            for y in range(0, self._maze.height):
                if self._maze.maze[y,x]:
                    self._display_surf.blit(self._block_surf,
                                            (x * self._scale, y * self._scale))
        self._display_surf.blit(self._door_surf,
                                (self._maze.exit_x * self._scale,
                                 self._maze.exit_y * self._scale))
        pygame.display.flip()

    def cleanup(self):
        pygame.quit()

    def run(self):
        if self.init() == False:
            self._running = False

        while( self._running ):
            pygame.event.pump()
            m = self.callback(self.position)
            self.move(self.position, m)
            self.render()

        self.cleanup()

    def move(self, pos, m):
        (x,y) = (pos.x, pos.y)
        # note:  x movement is inverted
        if m == 'N':
            y = y - 1
        elif m == 'S':
            y = y + 1
        elif m == 'E':
            x = x + 1
        elif m == 'W':
            x = x - 1
        if not self._maze.maze[y, x]:
            pos.x = x
            pos.y = y
        # stop if we get to the exit
        if (x,y) == (self._maze.exit_x, self._maze.exit_y):
            self._running = False
